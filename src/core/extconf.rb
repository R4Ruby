$:.unshift('lib')

require 'mkmf'

BASE_DIRS = [
  '/usr/lib/R',
  '/usr',
  '/usr/local',
]

def paths(type)
  BASE_DIRS.collect { |d| File.join(d, type.to_s) }
end
def include_paths; paths(:include); end
def library_paths; paths(:lib); end

EXT_NAME = 'r4ruby_core'

find_header('Rinterface.h', *include_paths)
find_library('R', nil, *library_paths)
dir_config(EXT_NAME)
create_header("#{EXT_NAME}.h")
create_makefile(EXT_NAME)
