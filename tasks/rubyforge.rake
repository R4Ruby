require('rake/contrib/rubyforgepublisher')

def rf_publisher
  rf_user = ENV['RUBYFORGE_USER']
  publisher = Rake::CompositePublisher.new
  publisher.add(Rake::SshDirPublisher.new("#{rf_user}@rubyforge.org", 
                                          "/var/www/gforge-projects/r4ruby/doc", 
                                          "doc"))
  publisher.add(Rake::SshDirPublisher.new("#{rf_user}@rubyforge.org",
                                          "/var/www/gforge-projects/r4ruby/",
                                          "web"))


  publisher
end

namespace :publish do
  task :web do
    rf_publisher.upload
  end
end

